from scipy.stats import norm
from csv import writer
import numpy as np

def generate_points_poz(num_points:int, szerokosc, dlugosc, przes_0x, przes_0y, przes_0z):
    szerokosc /= 2
    dlugosc /= 2

    distribution_x = norm(loc=(0+przes_0x), scale=szerokosc)
    distribution_y = norm(loc=(0+przes_0y), scale=dlugosc)
    distribution_z = norm(loc=(0+przes_0z), scale=0)

    x = distribution_x.rvs(size=num_points)
    y = distribution_y.rvs(size=num_points)
    z = distribution_z.rvs(size=num_points)

    points = zip(x, y, z)
    return points


def generate_points_pion(num_points:int, szerokosc, wysokosc, przes_0x, przes_0y, przes_0z):
    szerokosc /= 2
    wysokosc /= 2

    distribution_x = norm(loc=(0+przes_0x), scale=szerokosc)
    distribution_y = norm(loc=(0+przes_0y), scale=0)
    distribution_z = norm(loc=(0+przes_0z), scale=wysokosc)

    x = distribution_x.rvs(size=num_points)
    y = distribution_y.rvs(size=num_points)
    z = distribution_z.rvs(size=num_points)

    points = zip(x, y, z)
    return points


def generate_points_cylin(num_points:int, promien, wysokosc, przes_0x, przes_0y, przes_0z):
    wysokosc /= 2

    u = np.linspace(0, 2 * np.pi, num_points)
    x = (promien * np.cos(u)) + przes_0x
    y = (promien * np.sin(u)) + przes_0y


    distribution_z = norm(loc=(0+przes_0z), scale=wysokosc)
    z = distribution_z.rvs(size=num_points)

    points = zip(x, y, z)
    return points


if __name__ == '__main__':
    cloud_points_poz = generate_points_poz(10000, 1000, 1000, 0, 1000, 2000)
    cloud_points_pion = generate_points_pion(10000, 1000, 1000, 2000, 0, 0)
    cloud_points_cylin = generate_points_cylin(10000, 500, 1000, 0, 0 ,0)

    with open('points.xyz', 'w', encoding='utf-8', newline='\n') as csvfile:
        csvwriter = writer(csvfile)
        for p in cloud_points_poz:
            csvwriter.writerow(p)
        for p in cloud_points_pion:
            csvwriter.writerow(p)
        for p in cloud_points_cylin:
            csvwriter.writerow(p)
