import csv
from sklearn.cluster import KMeans
import numpy as np
from random import sample
from sklearn.cluster import DBSCAN
import pyransac3d as pyrsc

#------------RANSAC_3D--------------------------------------
def RANSAC_3D(liczba_iteracji, threshold, powierzchnia):
    najwieksza_liczba_inlierow = 0
    punkty_najwiekszej_liczby_inlierow = []
    vector_W_best = []
    D_best = 0

    for i in range(0, liczba_iteracji):
        indeksy = list(range(0,len(powierzchnia)-1))
        indeksy_ABC = sample(indeksy,3)
        punkty_ABC = powierzchnia[indeksy_ABC]

        punkt_A = punkty_ABC[0]
        punkt_B = punkty_ABC[1]
        punkt_C = punkty_ABC[2]

        vector_A = punkt_A - punkt_C
        vector_B = punkt_B - punkt_C

        vector_jedn_A = vector_A/np.linalg.norm(vector_A)
        vector_jedn_B = vector_B/np.linalg.norm(vector_B)
        vector_jedn_C = np.cross(vector_jedn_A, vector_jedn_B)

        vector_W = vector_jedn_C
        D = -np.sum(np.multiply(vector_W, punkt_C))

        distance_all_points = (vector_W[0]*powierzchnia[:,0] + vector_W[1]*powierzchnia[:,1] + vector_W[2]*powierzchnia[:,2] + D)/np.linalg.norm(vector_W)
        inliers = np.where(np.abs(distance_all_points) <= threshold)[0]
        liczba_inlierow = len(inliers)

        if liczba_inlierow > najwieksza_liczba_inlierow:
            najwieksza_liczba_inlierow = liczba_inlierow
            punkty_najwiekszej_liczby_inlierow = inliers
            vector_W_best = vector_W
            D_best = D

    distance_all_points_mean = np.abs(sum(distance_all_points)/len(distance_all_points))

    return [vector_W_best, distance_all_points_mean]

#------------Wczytanie_pliku_"points"----------------------
def punkty_xyz():
    with open('points.xyz', newline='') as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        for x, y, z in reader:
            yield (float(x), float(y), float(z))

lista_wspolrzednych = list(punkty_xyz())

#------------Algorytm_k-means_(dla_k=3)---------------------
X = np.array(lista_wspolrzednych)

clusterer = KMeans(n_clusters=3)
y_pred = clusterer.fit_predict(X)

powierzchnia_1 = X[y_pred == 0]
powierzchnia_2 = X[y_pred == 1]
powierzchnia_3 = X[y_pred == 2]

#------------Sprawdzenie_1------------------------------------
def sprawdzenie(Wynik_RANSAC, tolerancja_odleglosci, tolerancja_wketora_W):
    if Wynik_RANSAC[1] <= tolerancja_odleglosci:
        print("Powierzchnia jest płaszczyzną")

        if np.abs(Wynik_RANSAC[0][0]) <= tolerancja_wketora_W and np.abs(Wynik_RANSAC[0][1]) <= tolerancja_wketora_W and Wynik_RANSAC[0][2] != 0:
            print("Płaszczyzna pozioma")
            print("")
        elif (Wynik_RANSAC[0][0] != 0 or Wynik_RANSAC[0][1] != 0) and np.abs(Wynik_RANSAC[0][2]) <= tolerancja_wketora_W:
            print("Płaszczyzna pionowa")
            print("")

    else:
        print("Powierzchnia nie jest płaszczyzną")
        print("")


print("Powierzchnia 1:", end='\n')
Wynik_RANSAC_1 = RANSAC_3D(100, 1, powierzchnia_1)
print("Wektor normalny W: ", Wynik_RANSAC_1[0])
sprawdzenie(Wynik_RANSAC_1, 10, 0.05)

print("Powierzchnia 2:", end='\n')
Wynik_RANSAC_2 = RANSAC_3D(100, 1, powierzchnia_2)
print("Wektor normalny W: ", Wynik_RANSAC_2[0])
sprawdzenie(Wynik_RANSAC_2, 10, 0.05)

print("Powierzchnia 3:", end='\n')
Wynik_RANSAC_3 = RANSAC_3D(100, 1, powierzchnia_3)
print("Wektor normalny W: ", Wynik_RANSAC_3[0])
sprawdzenie(Wynik_RANSAC_3, 10, 0.05)

#------------Algorytm_DBSCAN--------------------------------------------
XX = np.array(lista_wspolrzednych)
db = DBSCAN(eps=400, min_samples=780).fit(XX)
labels = db.labels_
n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)
n_noise_ = list(labels).count(-1)
#print("Estimated number of clusters: %d" % n_clusters_)
#print("Estimated number of noise points: %d" % n_noise_)

powierzchnia_11 = XX[labels == 0]
powierzchnia_22 = XX[labels == 1]
powierzchnia_33 = XX[labels == 2]

#------------Dedykowane_funkcje_z_pakietu_pyransac3d--------------------
plane1 = pyrsc.Plane()
best_eq1, best_inliers1 = plane1.fit(powierzchnia_11, 1, 1, 100)

plane2 = pyrsc.Plane()
best_eq2, best_inliers2 = plane2.fit(powierzchnia_22, 1, 1, 100)

plane3 = pyrsc.Plane()
best_eq3, best_inliers3 = plane3.fit(powierzchnia_33, 1, 1, 100)


distance_all_points11 = (best_eq1[0]*powierzchnia_11[:,0] + best_eq1[1]*powierzchnia_11[:,1] + best_eq1[2]*powierzchnia_11[:,2] + best_eq1[3])/np.linalg.norm(best_eq1[0:3])
distance_all_points22 = (best_eq2[0]*powierzchnia_22[:,0] + best_eq2[1]*powierzchnia_22[:,1] + best_eq2[2]*powierzchnia_22[:,2] + best_eq2[3])/np.linalg.norm(best_eq2[0:3])
distance_all_points33 = (best_eq3[0]*powierzchnia_33[:,0] + best_eq3[1]*powierzchnia_33[:,1] + best_eq3[2]*powierzchnia_33[:,2] + best_eq3[3])/np.linalg.norm(best_eq3[0:3])

distance_all_points_mean11 = np.abs(sum(distance_all_points11) / len(distance_all_points11))
distance_all_points_mean22 = np.abs(sum(distance_all_points22) / len(distance_all_points22))
distance_all_points_mean33 = np.abs(sum(distance_all_points33) / len(distance_all_points33))

Wynik_RANSAC_11 = [np.array(best_eq1[0:3]), distance_all_points_mean11]
Wynik_RANSAC_22 = [np.array(best_eq2[0:3]), distance_all_points_mean22]
Wynik_RANSAC_33 = [np.array(best_eq3[0:3]), distance_all_points_mean33]

#------------Sprawdzenie_2----------------------------------------------
print("Powierzchnia 11:", end='\n')
print("Wektor normalny W: ", Wynik_RANSAC_11[0])
sprawdzenie(Wynik_RANSAC_11, 10, 0.05)

print("Powierzchnia 22:", end='\n')
print("Wektor normalny W: ", Wynik_RANSAC_22[0])
sprawdzenie(Wynik_RANSAC_22, 10, 0.05)

print("Powierzchnia 33:", end='\n')
print("Wektor normalny W: ", Wynik_RANSAC_33[0])
sprawdzenie(Wynik_RANSAC_33, 10, 0.05)

